Adaptation du logiciel "Avoir et être" inclus dans le lanceur d'applications Clicmenu de PragmaTICE.

Apprendre à conjuguer les verbes (auxiliaires) avoir et être aux temps les plus usités.

L'application peut être testée en ligne [ici](https://primtux.fr/applications/avoir-etre/index.html)
